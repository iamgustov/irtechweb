<?php
// Отвечает за вопрос

class Question {
    private $id;
    private $name;
    private $answerList;

    function __construct($name)
    {
        $this->id = null;
        $this->answerList = array();
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setAnswerList($answerList)
    {
        $this->answerList = $answerList;
    }

    public function getAnswerList()
    {
        return $this->answerList;
    }

    public function saveToBase(){
        R::begin();
        try{
            $question = R::dispense('question');
            $question->name = $this->name;

            $rbEntities = Helper::convertEntityToRb('answer', $this->answerList);

            foreach ($rbEntities as $rbEntity)
                $question->ownAnswerList[] = $rbEntity;

            $this->id = R::store($question);

            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            throw $ex;
        }
    }
} 