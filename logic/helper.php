<?php

class Helper {
    public static function loadEntity($entityName, $id = null){
        try{
            if ($id)
                return R::load($entityName, $id);
            else
                return R::getAll('select * from :entityName limit 1',array(
                    ':entityName' => $entityName
                ));
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function resetTestResults($id = null){
        R::begin();
        try{
            if (!$id)
                throw new Exception('User id is required..');

            $user = R::load('user', $id);
            R::trash($user);

            R::commit();
        }catch (Exception $ex){
            R::rollback();
            throw $ex;
        }
    }

    public static function getTest($id = null){
        try{
            if ($id)
                return R::load('test', $id);
            else
                return R::getAll('select * from test limit 1');
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function convertEntityToRb($entityName, $entityList){
        try{
            $result = array();

            foreach ($entityList as $entity){
                $result[] = R::load($entityName, $entity->getId());
            }

            return $result;
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function convertRbToEntity($entityName, $rbList, $entityFields){
        try{
            $result = array();

            return $result;
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function nukeTestAppStructure(){
        R::begin();
        try{
            // Не самое лучшее решение использовать nuke, т.к. удаляет все таблицы
            R::nuke();

            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            throw $ex;
        }
    }

    public static function getTetArrayStructureById($id = null){
        try{
            if (!$id)
                throw new Exception('Test id should not be null!');

            $test = R::load('test', $id);

            $testStructure = $test->export();

            $questionStructure = array();

            foreach ($test->ownQuestion as $question){
                $qStructure = $question->export();

                $answerStructure = array();
                foreach ($question->ownAnswer as $answer){
                    $answerStructure[] = $answer->export();
                }

                $qStructure['answers'] = $answerStructure;

                $questionStructure[] = $qStructure;
            }

            $testStructure['questions'] = $questionStructure;

            return $testStructure;

        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function getReportOne(){
        try{
            $result = array();

            $result = self::getReportThree();
            $result['userCount'] = R::count('user');

            return $result;
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function getReportTwo(){
        try{
            $marks = R::getAll(
                'select u.id as id, u.name as name, sum(ua.is_answered * a.mark) / ((select sum(mark)
                                                      from answer)) as mark
                    from user u, uanswer ua, answer a, uanswer_user uau, answer_uanswer aua
                    where ua.id = uau.uanswer_id
                          and ua.id = aua.uanswer_id
                          and u.id = uau.user_id
                          and a.id = aua.answer_id
                    group by u.id;'
            );

            return $marks;
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function getReportThree(){
        try{
            $totalCounts = R::getAll(
                'select u.id, u.name, sum(ua.is_answered * a.mark) as mark
                    from user u, uanswer ua, answer a, uanswer_user uau, answer_uanswer aua
                    where ua.id = uau.uanswer_id
                          and ua.id = aua.uanswer_id
                          and u.id = uau.user_id
                          and a.id = aua.answer_id
                    group by u.id
                    order by u.id, a.id;'
            );

            $users = R::getAll('select * from user');
            $questions = R::getAll('select * from question');

            $questionRows = array();
            foreach ($questions as $question){

                $userRows = array();
                $avgSum = 0;
                foreach ($users as $user) {
                    $userRow = R::getAll(
                        'SELECT user.id as userId, user.name as userName, question.id as questionId, question.name as questionName,
                              sum(answer.mark * uanswer.is_answered) as sum
                            FROM uanswer, user, question, uanswer_user, answer_uanswer, answer
                            WHERE question.id = answer.question_id
                                  and uanswer.id=answer_uanswer.uanswer_id
                                  and answer_uanswer.answer_id=answer.id
                                  and uanswer_user.user_id = user.id
                                  and uanswer_user.uanswer_id=uanswer.id
                                  and user.id = :uid
                                  and question.id = :qid
                            group by user.id, question.id',[':uid' => $user['id'], ':qid' => $question['id']]
                    );

                    $element = reset($userRow);
                    $userRows[] = $element['sum'];
                    $avgSum += $element['sum'];
                }

                $questionRows[] = array(
                    'questionName' => $question['name'],
                    'userMarks' => $userRows,
                    'avgSum' => $avgSum / count($userRows)
                );
            }

            return array(
                'userRows' => $questionRows,
                'totalCounts' => $totalCounts
            );
        }catch (Exception $ex){
           throw $ex;
        }
    }
} 