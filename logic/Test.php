<?php
// Отвечает за тест

class Test {
    private $id;
    private $name;
    private $questionList;

    public function __construct($name){
        $this->id = null;
        $this->name = $name;
        $this->questionList = array();
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function getQuestionList(){
        return $this->questionList;
    }

    public function setQuestionList($questionList){
        $this->questionList = $questionList;
    }

    public function saveToBase(){
        R::begin();
        try{
            $test = R::dispense('test');
            $test->name = $this->name;

            $rbEntities = Helper::convertEntityToRb('question', $this->questionList);

            foreach ($rbEntities as $rbEntity)
                $test->ownQuestionList[] = $rbEntity;

            $this->id = R::store($test);

            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            throw $ex;
        }
    }
}