<?php
// Отвечает за ответ на вопрос

class Answer {
    private $id;
    private $name;
    private $mark;

    function __construct($name, $mark)
    {
        $this->id = null;
        $this->mark = $mark;
        $this->name = $name;

        $this->saveToBase();
    }

    public function getMark()
    {
        return $this->mark;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function saveToBase(){
        R::begin();
        try{
            $answer = R::dispense('answer');

            $answer->name = $this->name;
            $answer->mark = $this->mark;

            $this->id = R::store($answer);

            R::commit();
        }catch(Exception $ex){

            R::rollback();
            throw $ex;
        }
    }
} 