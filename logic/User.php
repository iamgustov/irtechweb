<?php
// Отвечает за пользователей

class User {
    private $id;
    private $name;

    public function  __construct($name){
        $this->name = $name;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function saveToBase(){
        R::begin();
        try{
            $user = R::dispense('user');
            $user->name = $this->name;

            $this->id = R::store($user);

            R::commit();
        } catch (Exception $ex) {
            R::rollback();
            throw $ex;
        }
    }
} 