<?php

require_once __DIR__.'/../config/config.php';
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../orm/rb.php';
require_once __DIR__.'/../logic/helper.php';
require_once __DIR__.'/../logic/Answer.php';
require_once __DIR__.'/../logic/Question.php';
require_once __DIR__.'/../logic/Test.php';
require_once __DIR__.'/../logic/User.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

R::setup(DB_DRIVER.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);

$app = new Silex\Application();

// Session registration in Silex micro-framework
$app->register(new Silex\Provider\SessionServiceProvider());

// Here will be the test initialize action
$app->get('/api/test/init', function() {
    try{
        Helper::nukeTestAppStructure();

        $questionList = array();
        for ($j = 1; $j <= 10; $j++){
            $question = new Question('Вопрос №' . $j . '?');

            $answerList = array();
            for ($i = 1; $i <= 4; $i++){
                $answerList[] = new Answer('Ответ №' . $i, rand(0, 5));
            }

            $question->setAnswerList($answerList);
            $question->saveToBase();

            $questionList[] = $question;
        }

        $test = new Test('Тест №1');
        $test->setQuestionList($questionList);
        $test->saveToBase();

        return new Response(json_encode(array(
            'errorCode' => '0',
            'content' => 'Test was initialized successfully'
        )), 200);
    } catch (Exception $ex) {
        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

// Here will be action returns test structure
$app->get('/api/test', function(Request $request) {
    try{
        $testJSON = json_encode(Helper::getTetArrayStructureById(1));

        // Clear session for new user
        $request->getSession()->clear();

        return new Response(
            $testJSON, 200, array('Content-Type' => 'text/json')
        );
    }catch (Exception $ex){
        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

// Here will be action returns test structure
$app->post('/api/test', function(Request $request) use ($app) {
    R::begin();

    try{
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            //TODO refactoring here
            $userResults = json_decode($request->getContent());

            if (!$userResults->userName)
                throw new Exception('Username is required..');

            if (!$userResults->userAnswers)
                throw new Exception('User answers is required..');

            $createdAt = time();

            $user = new User($userResults->userName);
            $user->saveToBase();

            // Start session for user
            if ($user->getId())
                $app['session']->set('userId', $user->getId());

            $index = 0;

            foreach ($userResults->userAnswers as $answer){
                if ($answer !== null){
                    $answerEntity = Helper::loadEntity('answer', $index);
                    $userEntity = Helper::loadEntity('user', $user->getId());

                    $userAnswer = R::dispense('uanswer');
                    $userAnswer->isAnswered = $answer;
                    $userAnswer->createdAt = $createdAt;

                    $userEntity->sharedUanswerList[] = $userAnswer;
                    $answerEntity->sharedUanswerList[] = $userAnswer;

                    R::storeAll( [$userEntity, $answerEntity] );
                }

                $index++;
            }

            R::commit();

            return new Response(json_encode(array(
                'errorCode' => '0',
                'content' => 'User results have successfully requested..'
            )), 200);

        }else{
            throw new Exception('JSON expected..');
        }
    } catch(Exception $ex){
        R::rollback();

        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

// Here will be action returns test structure
$app->get('/api/test/report/{reportId}', function($reportId) {
    try{

        $reportJSON = '';

        switch ($reportId){
            case 1: $reportJSON = json_encode(Helper::getReportOne()); break;
            case 2: $reportJSON = json_encode(Helper::getReportTwo()); break;
            case 3: $reportJSON = json_encode(Helper::getReportThree()); break;
            default: throw new Exception('No report found..');
        }

        return new Response(
            $reportJSON, 200, array('Content-Type' => 'text/json')
        );
    }catch (Exception $ex){
        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

// Reset Test, по существу лучше бы сделать это через DELETE, но говорят IE8 не всегда отрабатывает
$app->post('/api/test/reset', function() use($app) {
   try{
       // Не по всем правилам rest, но не смертельно
       session_start();
       if ($app['session']->get('userId')){
           Helper::resetTestResults($app['session']->get('userId'));

           return new Response(json_encode(array(
               'errorCode' => '0',
               'content' => 'Your test results were successfully reseted..'
           )), 200);
       } else
           throw new Exception('No session found..');
   }catch (Exception $ex){
       return new Response(json_encode(array(
           'errorCode' => '1',
           'content' => $ex->getMessage()
       )), 200);
   }
});

$app->run();