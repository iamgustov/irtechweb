<!doctype html>
<html class="no-js" lang="en" ng-app="app">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Иртех | Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/xcharts.min.css" />
    <script src="js/vendor/modernizr.js"></script>
      <script src="js/xcharts.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
  </head>
  <body>
    <div class="row header">
      <div class="large-12 columns">
          <div class="large-6 columns logo">
            <img src="./img/logo.png" /> Инновационные решения для системы образования
          </div>
          <div class="large-6 columns menu text-right">

          </div>
      </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <div style="display: none;" id="successfullyLoggedIn" data-alert class="alert-box success">
                <span id="successfullyLoggedInMessage"></span>
                <a href="#" class="close">&times;</a>
            </div>
            <div style="display: none;" id="successfullyLoggedOut" data-alert class="alert-box warning">
                <span id="successfullyLoggedOutMessage"></span>
                <a href="#" class="close">&times;</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <div id="greeting" class="panel" ng-controller="StartController">
                <div>
                    <h4>Добро пожаловать в систему тестирования</h4>
                    <p>
                        Комплексная информационная система для современной школы. Позволяет и решать административные задачи, и вести мониторинг текущего учебного процесса, и наладить оперативное общение между всеми участниками этого процесса.
                    </p>
                    <p>
                        Родитель входит в NetSchool со своим персональным паролем и может видеть информацию только о своем ребенке (если у него в школе учится несколько детей, то о каждом из них).
                    </p>
                    <p>
                        Помимо дневника, родитель и учащийся имеет доступ в NetSchool к расписанию занятий, портфолио ученика, отчетам о его успеваемости (а также сравнительным отчетам по классу и параллели).
                    </p>

                    <a href="#" id="startTest" ng-click="startTest()" class="button">Начать тестирование</a>
                    <a href="#" id="startReportTwo" ng-click="startReportTwo()" class="button">Показать отчет №2</a>
                    <a href="#" id="startReportThree" ng-click="startReportThree()" class="button">Показать отчет №3</a>
                </div>
                <div id="reportTwo" class="panel testWrapper">
                    <h4>Отчет №2</h4>
                    <p>
                    <table class="fullWidth">
                        <thead>
                        <tr>
                            <th width="200">Респондент</th>
                            <th width="200">Средний балл</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="reportItem in reportTwo">
                            <td>{{ reportItem.name }}</td>
                            <td>{{ reportItem.mark }}</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div id="reportThree" class="panel testWrapper">
                    <h4>Отчет №3</h4>
                    <p>
                    <table class="fullWidth">

                        <tr>
                            <td width="200"></td>
                            <td width="200" ng-repeat="user in reportThree.totalCounts"><b>{{ user.name }}</b></td>
                            <td width="200"><b>Средний балл</b></td>
                        </tr>
                        <tr ng-repeat="reportItem in reportThree.userRows">
                            <td><b>{{ reportItem.questionName }}</b></td>
                            <td ng-repeat="mark in reportItem.userMarks">{{ mark }}</td>
                            <td>{{ reportItem.avgSum }}</td>
                        </tr>
                        <tr>
                            <td width="200"><b><a ng-click="submitTest()" href="#">Общее кол-во баллов</a></b></td>
                            <td width="200" ng-repeat="user in reportThree.totalCounts">{{ user.mark }}</td>
                            <td width="200"></td>
                        </tr>
                    </table>
                    </p>
                    <figure style="width: 100%; height: 300px;" id="myChart"></figure>
                </div>
            </div>
            <div ng-controller="TestController">
                <div id="userScope" class="panel testWrapper"  >
                    <h4>Введите Ваше имя</h4><br/>
                    <form name="test_form" ng-submit="submitName()">
                        <input type="text" name="some_name" ng-model="userName" required>
                        <input class="button" id="goToTest" type="submit" value="Перейти к тестированию" />
                    </form>
                </div>
                <div id="testScope" class="panel testWrapper" >
                    <h4>{{ test.name }}</h4><br/>
                    <form name="test_form" ng-submit="submitTest()">
                        <p ng-repeat="question in test.questions">
                            {{ question.name }}
                            <label ng-repeat="answer in question.answers" class="checkbox" for="{{answer.id}}">
                                <input type="checkbox" ng-model="answersList[answer.id]" ng-true-value="1" ng-false-value="0" id="{{answer.id}}" />
                                {{answer.name}}
                            </label></input>
                        </p>
                        <input class="button" type="submit" value="Завершить тестирование" />
                    </form>
                </div>
                <div id="reportOne" class="panel testWrapper">
                    <h4>Отчет №1</h4>
                    <p>
                    <table class="fullWidth">
                        <tr>
                            <td width="200"><b>Количество респондентов</b></td>
                            <td width="200">{{ reportOne.userCount }}</td>
                        </tr>
                        <tr ng-repeat="reportItem in reportOne.userRows">
                            <td>{{ reportItem.questionName }}</td>
                            <td>{{ reportItem.avgSum }}</td>
                        </tr>
                    </table>
                    </p>
                    <p><a class="button" ng-click="resetResults()" href="#">Сбросить результаты</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <hr/>
            <p>
                2014 &copy; Gate8
            </p>
        </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/gate8.js"></script>
  </body>
</html>
