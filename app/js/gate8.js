jQuery(document).ready(function($){
    $(document).foundation();
});

var app = angular.module("app", []);

var testPath = '../api/index.php/api/test';
var reportPath = '../api/index.php/api/test/report/';
var resetPath = '../api/index.php/api/test/reset';

var StartController = function StartController($scope, $http){
    $scope.startTest = function(){
        $('#greeting').hide();
        $('#userScope').show();
    }

    $scope.startReportTwo = function(){

        $http.get(reportPath+'2').success(function(data){
            $scope.reportTwo = data;
        });

        $('#reportTwo').toggle();
    }

    $scope.startReportThree = function(){
        $http.get(reportPath+'3').success(function(data){
            $scope.reportThree = data;

            var points = new Array();
            var i = 0;
            $scope.reportThree.totalCounts.forEach(function(element){
                points.push({
                    "x": $scope.reportThree.totalCounts[i++].name,
                    "y": element.mark
                });
            });

            var main = [{
                "className" : ".pizza",
                "data" : points
            }];

            var data = {
                "xScale": "ordinal",
                "yScale": "ordinal",
                "main": main
            };

            $scope.chartData = data;

            $scope.submitTest = function(){
                var myChart = new xChart('bar', $scope.chartData, '#myChart');
            }
        });

        $('#reportThree').toggle();
    }
};

var TestController = function TestController($scope, $http, $filter, $route){

    $scope.userName = '';
    $scope.answersList = [];

    $http({method: 'GET', url: testPath}).success(function(data){
        $scope.test = data;

        $scope.test.questions.forEach(function(question) {
            question.answers.forEach(function(answer) {
                $scope.answersList[answer.id] = "0";
            });
        });
    });

    $scope.submitName = function() {
        // TODO something here ;)

        $('#userScope').hide();
        $('#testScope').show();
    }

    $scope.resetResults = function() {
        $http.post(resetPath).success(function(data){

            $http.get(reportPath+'1').success(function(data){
                $scope.reportOne = data;
            });

            alert(data.content);
        });
    }

    $scope.submitTest = function(){
        var userResults = {
            userName: $scope.userName,
            userAnswers: $scope.answersList
        }

        $http.post(testPath, JSON.stringify(userResults)).success(function(data){
            $('#testScope').hide();

            $http.get(reportPath+'1').success(function(data){
                $scope.reportOne = data;
            });

            $('#reportOne').show();
        });

    }
};